package com.gitlab.mathhelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class Matrix {

    Logger logger = LoggerFactory.getLogger(Matrix.class);

    int rows;
    int columns;
    int[] cells = new int[rows * columns];

    public Matrix(){}

    public Matrix(int rows, int columns, int[] cells){
        this.rows = rows;
        this.columns = columns;

        if(cells.length != rows*columns) {
            String msg = "Can't create matrix, size: ";
            msg += rows + "*" + columns;
            msg += " array size: ";
            msg += cells.length;
            logger.warn(msg);
            throw new IndexOutOfBoundsException();
        }

        this.cells = cells;

        String msg = "Create matrix, size: " + rows + "*" + columns;
        logger.info(msg);
    }

    public Matrix(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.cells = new int[rows * columns];

        String msg = "Create zero matrix, size: " + rows + "*" + columns;
        logger.info(msg);
    }

    public Matrix multiply(Matrix secondMatrix){
        logForMatrices(secondMatrix);

        if(this.columns != secondMatrix.rows){
            String errorMsg = "Can't multiply matrices " + this.columns + "!=" + secondMatrix.rows;
            logger.warn(errorMsg);
            throw new ArithmeticException();
        }

        Matrix result = new Matrix(this.rows, secondMatrix.columns);

        for (int i = 0; i < this.rows; i++){
            for(int j = 0; j < secondMatrix.columns; j++) {
                for(int k = 0; k < this.columns; k++) {
                    result.cells[i * result.columns + j] +=
                            cells[i*this.columns + k] * secondMatrix.cells[k*secondMatrix.columns + j];
                }
            }
        }

        logForResultMatrix(result);

        logger.info("Successfully multiply");
        return result;
    }

    public Matrix multiply(Matrix secondMatrix, int q){
        logForMatrices(secondMatrix);

        if(this.columns != secondMatrix.rows){
            throw new ArithmeticException();
        }

        Matrix result = new Matrix(this.rows, secondMatrix.columns);

        for (int i = 0; i < this.rows; i++){
            for(int j = 0; j < secondMatrix.columns; j++) {
                for(int k = 0; k < this.columns; k++) {
                    result.cells[i * result.columns + j] +=
                            cells[i*this.columns + k] * secondMatrix.cells[k*secondMatrix.columns + j];
                }
                result.cells[i * result.columns + j] = result.cells[i * result.columns + j] % q;
            }
        }

        logForResultMatrix(result);

        logger.info("Successfully multiply");
        return result;
    }

    private void logForMatrices(Matrix secondMatrix) {
        String msg = "Multiply matrices ";

        String firstMatrixInfo = " First matrix " + logInfoAboutMatrix(this);
        msg += firstMatrixInfo;

        String secondMatrixInfo = " Second matrix " + logInfoAboutMatrix(secondMatrix);
        msg += secondMatrixInfo;

        logger.debug(msg);
    }

    private void logForResultMatrix(Matrix result){
        String resultMatrixInfo = " Result matrix " + logInfoAboutMatrix(result);
        logger.debug(resultMatrixInfo);
    }

    private String logInfoAboutMatrix(Matrix matrix){
        String result = "size: " + matrix.rows + "*" + matrix.columns + " \n";

        for (int i = 0; i < matrix.rows; i++){
            for(int j = 0; j < matrix.columns; j++) {
                result = result.concat(matrix.cells[i * matrix.columns + j] + " ");
            }
            result = result.concat("\n");
        }

        return result;
    }


    public void zeroing(){
        for(int i = 0; i < rows * columns ; i++){
            this.cells[i] = 0;
        }
    }

    public boolean isEqualZeroMatrix(){
        for(int i = 0; i < rows*columns; i++){
            if(this.cells[i] != 0)
                return false;
        }
        return true;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public int[] getCells() {
        return cells;
    }

    public String getCellsString() { return Arrays.toString(cells); }

    public void setMatrix(int rows, int columns, int[] cells) throws IndexOutOfBoundsException {
        this.rows = rows;
        this.columns = columns;

        if(cells.length != rows*columns) {
            throw new IndexOutOfBoundsException();
        }

        this.cells = cells;
    }


}
