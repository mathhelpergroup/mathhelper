package com.gitlab.mathhelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CodeByCodingMatrix {
    Logger logger = LoggerFactory.getLogger(CodeByCodingMatrix.class);

    int rows;
    int columns;
    int codeSize;
    List<Matrix> resultCode = new ArrayList<>();

    public CodeByCodingMatrix(Matrix A, int q){
        rows = A.getRows();
        columns = A.getColumns();

        Matrix result;
        Matrix generated;
        int numberOfVectors =(int)Math.pow(q, rows);
        codeSize = numberOfVectors;
        for(int i = 0; i < numberOfVectors; i++){
            //генерация вектора
            generated = generateVec(i, rows,q);
            result = generated.multiply(A, q);
            resultCode.add(result);
        }

        logger.info("Generated Code by Coding Matrix");
    }

    private Matrix generateVec(int number, int k, int q){
        int[] result = new int[k];
        for (int i = k-1; i >= 0; i--){
            result[i] = number%q;
            number = number/q;
        }
        return new Matrix(1, k, result);
    }

    public List<Matrix> getResultCode(){
        return resultCode;
    }

    public int getCodeSize(){
        return codeSize;
    }
}
