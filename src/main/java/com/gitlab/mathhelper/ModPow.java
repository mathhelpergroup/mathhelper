package com.gitlab.mathhelper;
import java.math.BigInteger;

public class ModPow {
    BigInteger mod;
    BigInteger pow;
    BigInteger base;

    public ModPow(BigInteger mod, BigInteger pow, BigInteger base) {
        this.mod = mod;
        this.pow = pow;
        this.base = base;
    }

    public BigInteger count() {
        return base.modPow(pow, mod);
    }
}
