package com.gitlab.mathhelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SquareMatrix extends Matrix{
    Logger logger = LoggerFactory.getLogger(SquareMatrix.class);

    private int size;

    public SquareMatrix(int size, int[] cells){
        this.size = size;
        this.rows = size;
        this.columns = size;
        if(cells.length != size*size) {
            String msg = "Can't create square matrix, size: ";
            msg += rows + "*" + columns;
            msg += " array size: ";
            msg += cells.length;
            logger.warn(msg);
            throw new IndexOutOfBoundsException();
        }
        this.cells = cells;

        String msg = "Create square matrix, size: " + rows + "*" + columns;
        logger.info(msg);
    }

    public SquareMatrix(int size) {
        this.size = size;
        this.rows = size;
        this.columns = size;
        this.cells = new int[size * size];
    }

    public SquareMatrix inverseMatrix(){
        int []tmpCells = new int[rows * columns];

        int determinant = this.getDeterminant();
        if (determinant != 0){
            for (int i = 0; i < rows; i++){
                for (int j = 0; j < columns; j++){
                    tmpCells[i*rows + j] = this.removeRowAndColumn(i,j).getDeterminant() / determinant;
                    if ((i+j) % 2 != 0) tmpCells[i*rows + j] *= -1;
                }
            }

            SquareMatrix tmpSquareMatrix = new SquareMatrix(this.size, tmpCells);
            tmpSquareMatrix.transpose();

            logger.info("Success inverse matrix");
            return tmpSquareMatrix;
        } else {
            logger.warn("Can't inverse matrix: Determinant = 0");
            throw new ArithmeticException();
        }
    }

    public double[] inverseMatrixDouble() {
        double []tmpCells = new double[rows * columns];

        int determinant = this.getDeterminant();
        if (determinant != 0){
            for (int i = 0; i < rows; i++){
                for (int j = 0; j < columns; j++){
                    tmpCells[i*rows + j] = this.removeRowAndColumn(i,j).getDeterminant();
                    tmpCells[i*rows + j] = tmpCells[i*rows + j] / determinant;
                    if ((i+j) % 2 != 0) tmpCells[i*rows + j] *= -1;
                }
            }
            logger.info("Success inverse matrix double");
            return tmpCells;
        } else {
            logger.warn("Can't inverse matrix: Determinant = 0");
            throw new ArithmeticException();
        }
    }

    public void multiplyOnNumber(int number){
        for (int i=0; i<rows*columns; i++){
            cells[i] *= number;
        }
    }

    public void transpose(){
        int index;
        int indexForSwap;
        int tmpCell;
        for(int i = 0; i < size; i++) {
            index = i * (size + 1);
            for (int j = 1; j < size - i; j++){
                index += 1;
                if (index % size == 0) break;
                indexForSwap = index - j + j*size;

                //swap
                tmpCell = cells[index];
                cells[index] = cells[indexForSwap];
                cells[indexForSwap] = tmpCell;
            }
        }

    }

    public SquareMatrix removeRowAndColumn(int row, int column){
        int tmpRows = this.rows - 1;
        int tmpColumns = this.columns - 1;
        int tmpSize = this.size - 1;
        int []tmpCells = new int[tmpRows * tmpColumns];
        int index;
        int z = 0;
        for(int i = 0 ; i < size; i++){
            for(int j = 0; j < size; j++){
                index = i*size + j;
                if((index/size == row) || (index%size == column)) continue;
                tmpCells[z] = cells[index];
                z++;
            }
        }
        return new SquareMatrix(tmpSize, tmpCells);
    }

    public int getDeterminant(){
        if (size == 1) return cells[0];
        else if (size == 2) return cells[0]*cells[3] - cells[1]*cells[2];
        else {

            int tmpDeterminant = 0;
            int index;
            for (int i = 0; i < size; i++) {
                int tmpLine = 1;
                index = i;
                for (int j = 0; j < size; j++) {
                    tmpLine *= cells[index];
                    if ((index + 1) % size == 0) {
                        index++;
                    } else {
                        index += size + 1;
                    }
                }
                tmpDeterminant += tmpLine;
            }

            for (int i = 0; i < size; i++) {
                int tmpLine = 1;
                index = i;
                for (int j = 0; j < size; j++) {
                    tmpLine *= cells[index];
                    if ((index) % size == 0) {
                        index += 2 * size - 1;
                    } else {
                        index += size - 1;
                    }
                }
                tmpDeterminant -= tmpLine;
            }
            return tmpDeterminant;
        }
    }

    public void setUnitMatrix(){
        this.zeroing();

        for(int i = 0; i < this.size; i++){
            int index = i + i*this.size;
            this.cells[index] = 1;
        }
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}

