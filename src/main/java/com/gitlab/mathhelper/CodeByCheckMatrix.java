package com.gitlab.mathhelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CodeByCheckMatrix {
    Logger logger = LoggerFactory.getLogger(CodeByCheckMatrix.class);

    List<Matrix> resultCode = new ArrayList<>();
    public CodeByCheckMatrix(Matrix A, int q){

        int n = A.getColumns();
        int k = A.getRows();

        Matrix result;
        Matrix generated;

        int numberOfVectors =(int)Math.pow(q, n);
        for(int i = 0; i < numberOfVectors; i++){
            generated = generateVec(i,n,q);
            result = A.multiply(generated, q);

            if(result.isEqualZeroMatrix()){
                resultCode.add(generated);
            }
        }

        logger.info("Generated Code by Check Matrix");
    }

    private Matrix generateVec(int number, int k, int q){
        int[] result = new int[k];
        for (int i = k-1; i >= 0; i--){
            result[i] = number%q;
            number = number/q;
        }
        return new Matrix(k, 1, result);
    }

    public List<Matrix> getResultCode(){
        return resultCode;
    }

    public int getCodeSize(){
        return resultCode.size();
    }
}
