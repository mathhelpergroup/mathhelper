package com.gitlab.mathhelper.controller;


import com.gitlab.mathhelper.CodeByCodingMatrix;
import com.gitlab.mathhelper.Matrix;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
public class ByCodingMatrixController {
    @GetMapping("/codingMatrix")
    public String main(){
        return "codingMatrix/mainCodingMatrix";
    }

    @PostMapping(value = "/codingMatrix", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> getCode(@RequestParam Map<String, String> allParams, Map<String, Object> model){
        int columns1 = Integer.parseInt(allParams.get("columns1"));
        int rows1 = Integer.parseInt(allParams.get("rows1"));
        int q = Integer.parseInt(allParams.get("q"));
        int [] cells1 = new int[columns1*rows1];

        for(int i = 0; i < rows1; i++){
            for(int j = 0; j < columns1; j++){
                cells1[i*columns1 + j] = Integer.parseInt(allParams.get(i + "_" + j + "_1"));
            }
        }
        Matrix matrix1 = new Matrix(rows1, columns1, cells1);

        CodeByCodingMatrix code = new CodeByCodingMatrix(matrix1, q);

        Map<String, Object> response = new HashMap<>();
        response.put("size", code.getCodeSize());

        List<String> codeLines = new ArrayList<>();
        for (Matrix m : code.getResultCode()) {
            codeLines.add(m.getCellsString());
        }

        response.put("codeLines", codeLines);

        return response;
    }
}
