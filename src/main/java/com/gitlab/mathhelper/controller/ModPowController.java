package com.gitlab.mathhelper.controller;

import com.gitlab.mathhelper.ModPow;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigInteger;
import java.util.Map;

@Controller
public class ModPowController {

    @GetMapping("/modPow")
    public String main(Map<String, Object> model){
        model.put("result", "result will be here");
        return "modPow/mainModPow";
    }

    @PostMapping("/modPow")
    public String modPow(@RequestParam int number,
                         @RequestParam int degree,
                         @RequestParam int mod,
                         Map<String, Object> model){
        if((number == 0) && (degree == 0) && (mod==0)){
            model.put("result", "Error: 0^0 mod 0 undefined");
            return "modPow/mainModPow";
        }
        BigInteger bigNumber = BigInteger.valueOf(number);
        BigInteger bigDegree = BigInteger.valueOf(degree);
        BigInteger bigMod = BigInteger.valueOf(mod);

        ModPow modPow = new ModPow(bigMod, bigDegree, bigNumber);
        BigInteger result = modPow.count();

        model.put("result", result);
        return "modPow/mainModPow";
    }
}
