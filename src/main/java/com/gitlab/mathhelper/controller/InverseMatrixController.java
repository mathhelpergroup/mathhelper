package com.gitlab.mathhelper.controller;

import com.gitlab.mathhelper.SquareMatrix;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class InverseMatrixController {

    @GetMapping("/inverseMatrix")
    public String main(){
        return "inverseMatrix/mainInverse";
    }

    @PostMapping(value = "/inverseMatrix", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> inverse(@RequestParam Map<String, String> allParams, Map<String, Object> model){
        int size = Integer.parseInt(allParams.get("size"));
        int [] cells = new int[size*size];

        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                cells[i*size + j] = Integer.parseInt(allParams.get(i + "_" + j + "_1"));
            }
        }
        SquareMatrix matrix = new SquareMatrix(size, cells);
        double [] result = new double[size*size];
        Map<String, Object> response = new HashMap<>();

        try {
            result = matrix.inverseMatrixDouble();
            response.put("err", 0);
        } catch (ArithmeticException e){
            response.put("err", 1);
            return response;
        }
        response.put("size", size);
        response.put("cells", result);

        return response;
    }
}

