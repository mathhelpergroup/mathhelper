package com.gitlab.mathhelper.controller;

import com.gitlab.mathhelper.Matrix;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class MultiplyMatrixController {
    @GetMapping("/multiplyMatrix")
    public String main(){
        return "multiplyMatrix/mainMultiply";
    }

    @PostMapping(value = "/multiplyMatrix", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> multiply(@RequestParam Map<String,String> allParams, Map<String, Object> model){
        int columns1 = Integer.parseInt(allParams.get("columns1"));
        int rows1 = Integer.parseInt(allParams.get("rows1"));
        int [] cells1 = new int[columns1*rows1];

        for(int i = 0; i < rows1; i++){
            for(int j = 0; j < columns1; j++){
                cells1[i*columns1 + j] = Integer.parseInt(allParams.get(i + "_" + j + "_1"));
            }
        }
        Matrix matrix1 = new Matrix(rows1, columns1, cells1);

        int columns2 = Integer.parseInt(allParams.get("columns2"));
        int rows2 = Integer.parseInt(allParams.get("rows2"));
        int [] cells2 = new int[columns2*rows2];

        for(int i = 0; i < rows2; i++){
            for(int j = 0; j < columns2; j++){
                cells2[i*columns2 + j] = Integer.parseInt(allParams.get(i + "_" + j + "_2"));
            }
        }
        Matrix matrix2 = new Matrix(rows2, columns2, cells2);

        Matrix result = new Matrix(2,2);
        Map<String, Object> response = new HashMap<>();

        try {
            result = matrix1.multiply(matrix2);
            response.put("err", 0);
        } catch (ArithmeticException e){
            response.put("err", 1);
            return response;
        }

        response.put("rows", result.getRows());
        response.put("columns", result.getColumns());
        response.put("cells", result.getCells());

        return response;
    }
}