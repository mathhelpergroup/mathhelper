function renderCode(size, codeLines) {
    let text = "Code size: " + size + "<br/>";

    text += codeLines.map(function(a) {return a.replace('[', '').replace(']', '')}).join("<br/>");
    text += "<br/>";

    return text
}

module.exports = renderCode;
