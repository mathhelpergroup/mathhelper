function renderMatrix(rows, cols, matrixArray) {
    let text = '<table border="1">';

    for (let i = 0; i < rows; ++i) {
        text += "<tr>";
        for (let j = 0; j < cols; ++j) {
            let index = i * cols + j;
            text += '<td>' + matrixArray[index] +'</td>';
        }
        text += "</tr>";
    }

    text += '</table>';

    return text;
}

module.exports = renderMatrix;
