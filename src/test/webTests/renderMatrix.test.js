const renderMatrix = require('../../main/resources/static/RenderMatrix');

test("Render Matrix", function(){
    expect(renderMatrix(2, 3, [1,2,3,4,5,6]))
        .toEqual('<table border="1"><tr><td>1</td><td>2</td><td>3</td></tr><tr><td>4</td><td>5</td><td>6</td></tr></table>');

    expect(renderMatrix(2, 2, [1,0,0,1]))
        .toEqual('<table border="1"><tr><td>1</td><td>0</td></tr><tr><td>0</td><td>1</td></tr></table>');

    expect(renderMatrix(1, 10, [0,1,2,3,4,5,6,7,8,9]))
        .toEqual('<table border="1"><tr><td>0</td><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td><td>7</td><td>8</td><td>9</td></tr></table>');
});
