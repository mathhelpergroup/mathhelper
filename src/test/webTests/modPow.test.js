const fastModularExponentiation = require('../../main/resources/static/ModPow');

test("Modular exponentiation", function(){
    expect(fastModularExponentiation(3,4,10)).toEqual(1);
    expect(fastModularExponentiation(4,2,2)).toEqual(0);
    expect(fastModularExponentiation(7,1,10)).toEqual(7);
});

