import com.gitlab.mathhelper.CodeByCodingMatrix;
import com.gitlab.mathhelper.Matrix;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class CodeByCodingMatrixTests {
    @Test
    public void testSize(){
        int[] C = {
                1,1,0,0,2,
                1,2,1,1,0,
                1,0,0,2,2,
                2,1,1,0,1
        };
        int k = 4, n= 5, q = 3;

        Matrix matrixC = new Matrix(k, n, C);
        CodeByCodingMatrix codeByCodingMatrix = new CodeByCodingMatrix(matrixC, q);

        Assert.assertEquals(81,codeByCodingMatrix.getCodeSize());
    }

    @Test
    public void Test(){
        int[] C = {1,1,0,0,1,
                1,0,1,1,0,
                1,0,0,1,0};
        int k = 3, n= 5, q = 2;

        Matrix matrixC = new Matrix(k, n, C);
        CodeByCodingMatrix codeByCodingMatrix = new CodeByCodingMatrix(matrixC, q);
        List<Matrix> resultCode = codeByCodingMatrix.getResultCode();

        Assert.assertArrayEquals( new int[] {0,0,0,0,0}, resultCode.get(0).getCells());
        Assert.assertArrayEquals( new int[] {1,0,1,1,0}, resultCode.get(2).getCells());
        Assert.assertArrayEquals( new int[] {1,1,1,0,1}, resultCode.get(7).getCells());
    }
}
