import com.gitlab.mathhelper.SquareMatrix;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.fail;

public class SquareMatrixTests {
    @Test
    public void checkBounds(){
        try{
            SquareMatrix squareMatrix = new SquareMatrix(3, new int[] {1,2,3,4});
            fail("Expected exception");
        } catch (IndexOutOfBoundsException ignore){

        } catch (Exception e){
            fail(e.getMessage());
        }
    }

    @Test
    public void determinant(){
        int[] cells = new int[]{
                1,2,3,
                4,5,6,
                7,8,9
        };
        SquareMatrix squareMatrix = new SquareMatrix(3, cells);
        Assert.assertEquals(0, squareMatrix.getDeterminant());

        int[] cells1 = new int[]{
                2,4,
                6,1
        };
        SquareMatrix squareMatrix1 = new SquareMatrix(2, cells1);
        Assert.assertEquals(-22,squareMatrix1.getDeterminant());
    }

    @Test
    public void inverseMatrix(){
        int [] cells = new int[]{
                1,2,3,
                4,5,6,
                7,8,1
        };
        SquareMatrix squareMatrix = new SquareMatrix(3, cells);
        int [] result = squareMatrix.inverseMatrix().getCells();

        int [] expected = new int[]{
                -1, 0, 0,
                1,0,0,
                0,0,0
        };

        for(int i = 0; i<9; i++){
            Assert.assertEquals(expected[i], result[i]);
        }
    }

    @Test
    public void inverseMatrixDouble(){
        int [] cells = new int[]{
                2,3,
                -1,1
        };

        double [] expected = new double[]{
                0.2,-0.6,
                0.2, 0.4
        };

        SquareMatrix matrix = new SquareMatrix(2, cells);
        double[] result = matrix.inverseMatrixDouble();
        for(int i = 0; i < 4; i++){
            Assert.assertEquals(expected[i], result[i], 3);
        }
    }

    @Test
    public void testUnitMatrix(){
        SquareMatrix matrix = new SquareMatrix(3);
        matrix.setUnitMatrix();

        int [] result = matrix.getCells();

        int [] expected = new int[9];
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if(i==j)
                    expected[i*3+j] = 1;
            }
        }

        for(int i = 0; i < 9; i++){
            Assert.assertEquals(expected[i], result[i]);
        }
    }
}
