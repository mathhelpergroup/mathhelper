import com.gitlab.mathhelper.ModPow;
import org.junit.Assert;
import org.junit.Test;
import java.math.BigInteger;

public class ModPowTests {

    @Test
    public void standartTest(){
        ModPow modPow = new ModPow(BigInteger.valueOf(3),BigInteger.valueOf(4),BigInteger.valueOf(10));
        Assert.assertEquals(BigInteger.valueOf(1), modPow.count());
    }

    @Test(expected = ArithmeticException.class)
    public void checkException() throws ArithmeticException{
        ModPow modPow = new ModPow(BigInteger.valueOf(0),BigInteger.valueOf(0),BigInteger.valueOf(0));
        modPow.count();
    }
    //java.lang.ArithmeticException
}
