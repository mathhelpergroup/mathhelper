import com.gitlab.mathhelper.CodeByCheckMatrix;
import com.gitlab.mathhelper.Matrix;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class CodeByCheckMatrixTest {
    @Test
    public void standartCheck(){
        int B[] = {
                0,1,1,1,0,0,
                1,1,1,0,1,0,
                1,0,1,0,0,1
        };
        int n = 6, k =3, q = 2;
        Matrix matrix = new Matrix(k,n,B);

        CodeByCheckMatrix findCode = new CodeByCheckMatrix(matrix, q);

        List<Matrix> resultCode = findCode.getResultCode();
        Assert.assertArrayEquals(new int[]{0,0,0,0,0,0}, resultCode.get(0).getCells());
        Assert.assertArrayEquals(new int[]{0, 1, 0, 1, 1, 0}, resultCode.get(2).getCells());
        Assert.assertArrayEquals(new int[]{1, 1, 1, 0, 1, 0}, resultCode.get(7).getCells());

    }

    @Test
    public void checkCodeSize(){
        int B[] = {
                0,1,1,1,0,0,
                1,1,1,0,1,0,
                1,0,1,0,0,1
        };
        int n = 6, k =3, q = 2;
        Matrix matrix = new Matrix(k,n,B);

        CodeByCheckMatrix findCode = new CodeByCheckMatrix(matrix, q);

        Assert.assertEquals(8, findCode.getCodeSize());
    }
}
