import com.gitlab.mathhelper.Matrix;
import org.junit.Test;

import static org.junit.Assert.*;


public class MatrixTests {

    @Test
    public void checkBoundsInConstructor(){
        int[] A = new int[]{
                1,3,5,
                2,1,7
        };

        try{
            Matrix matrix = new Matrix(3,3, A);
            fail("Expected IndexOutOfBoundsException");
        } catch (IndexOutOfBoundsException ignore){

        } catch (Exception e){
            fail(e.getMessage());
        }
    }

    @Test
    public void checkBoundsInMultiply(){
        int [] A = new int[]{
                1,0,0,3,
                0,1,0,17,
                0,0,1,56
        };

        int[] B = {1,1,0,0,1,
                1,0,1,1,0,
                1,0,0,1,0};

        Matrix matrixB = new Matrix(3, 5, B);
        Matrix matrixA = new Matrix(3,4, A);

        try{
            matrixA.multiply(matrixB);
            fail("Expected ArithmeticException");
        } catch (ArithmeticException ignore){

        } catch (Exception e){
            fail(e.getMessage());
        }
    }

    @Test
    public void checkMultiply(){
        int [] A = new int[]{
                1,0,0,3,
                0,1,0,17,
                0,0,1,56
        };

        int [] B = new int []{
                2,2,1,
                55,16,9,
                1,3,4
        };
        Matrix matrixA = new Matrix(3,4, A);
        Matrix matrixB = new Matrix(3,3, B);
        //matrixB.multiply(matrixA);

        int [] expectedArray = new int[]{
                2,2,1,96,
                55,16,9,941,
                1,3,4,278
        };

        Matrix result = matrixB.multiply(matrixA);
        assertEquals(3, result.getRows());
        assertEquals(4, result.getColumns());
        assertArrayEquals(expectedArray, result.getCells());
    }

    @Test
    public void checkMultiplyWithQ(){
        int[] A = {1,1,0,0,1,
                1,0,1,1,0,
                1,0,0,1,0};
        int k = 3, n= 5, q = 2;

        Matrix matrixA = new Matrix(k, n, A);

        int [] B = {
                1,1,0
        };

        Matrix matrixB = new Matrix(1,3,B);

        Matrix result = matrixB.multiply(matrixA, 2);

        assertEquals(1, result.getRows());
        assertEquals(5, result.getColumns());

        int [] expectedArray = new int[]{
                0,1,1,1,1
        };

        assertArrayEquals(expectedArray, result.getCells());
    }

    @Test
    public void checkBoundsInSetMatrix(){
        int[] A = new int[]{
                1,3,5,
                2,1,7,
                2,14,55
        };

        Matrix matrix = new Matrix(3,3, A);

        int [] B = new int[]{
                2,2,1,
                5,5,6
        };

        try{
            matrix.setMatrix(2,2,B);
            fail("Expected IndexOutOfBoundsException");
        } catch (IndexOutOfBoundsException ignore){

        } catch (Exception e){
            fail(e.getMessage());
        }
    }
}
